# README #

### Prerequisites for the deploy script ###
* cmake installed
* luajit and lpeg installed

### How to setup and run the deploy script ###

* Edit the $BASE_DIR variable in deploy.sh. This is the base directory where everything is installed
* chmod u+x deploy.sh
* ./deploy.sh