#!/bin/bash

set -e

BASEDIR=/home/martin/sgxmusl-dfsan
LLVMSRC=$BASEDIR/llvm-src
LLVMBUILD=$BASEDIR/llvm-build
LUAGENDIR=$BASEDIR/sgx-musl-luagen
SGXSRC=$BASEDIR/sgx-musl

echo "0: creating the directory $BASE_DIR ..."
rm -rf $BASEDIR
mkdir $BASEDIR

echo "1: cloning the LLVM project ..."
git clone git@bitbucket.org:dimakuv/llvm-sgxmusl.git $LLVMSRC
cd $LLVMSRC
git checkout dfsan-sgxmusl

echo "2: cloning clang ..."
cd $LLVMSRC/tools
git clone https://github.com/llvm-mirror/clang.git $LLVMSRC/tools/clang
cd $LLVMSRC/tools/clang
git checkout 7e231a28806b135495717a4472e4b2d843d48310

echo "3: cloning compiler-rt ..."
git clone git@bitbucket.org:dimakuv/compiler-rt-sgxmusl.git $LLVMSRC/projects/compiler-rt
cd $LLVMSRC/projects/compiler-rt
git checkout dfsan-sgxmusl

echo "4: cloning sgx-musl ..."
git clone git@sereca.cloudandheat.com:sereca/sgx-musl.git $SGXSRC
cd $SGXSRC
git checkout dfsan

echo "5: cloning luagen ..."
git clone git@sereca.cloudandheat.com:sereca/sgx-musl-luagen.git $LUAGENDIR
cd $LUAGENDIR
git checkout dfsan_tainting

echo "6: generating rawsyscalls.c for sgx-musl..."
cd $LUAGENDIR
luajit -O3 -lluarocks.loader headerparse.lua $SGXSRC/src/internal/syscallwrappers.h
cp rawsyscalls.c $SGXSRC/src/misc/

echo "7: building SGX-enabled LLVM..."
mkdir $LLVMBUILD
cd $LLVMBUILD
cmake -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON -DLLVM_TARGETS_TO_BUILD=X86 $LLVMSRC
make -j4

echo "8: installing async version of sgx-musl to $SGXDIR/build/ ..."
cd $SGXSRC
./configure --prefix=$SGXSRC/build --enable-wrapper=all --disable-sgx-hw --clangdir=$LLVMBUILD/bin --sanitize --llvmdir=$LLVMSRC
make -j4
make install

echo "DONE"
